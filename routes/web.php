<?php
Route::group(['prefix' => '/admin'], function () {

  Route::get('/logout','Admin\AuthController@getLogout');
  Route::get('/login','Admin\AuthController@getLogin');
  Route::post('/login','Admin\AuthController@postLogin');
  Route::get('/Error/Page/NotFound', function() {
      return view('admin.404');
  });


  Route::group(['middleware' => 'auth:admin'] , function () {

    Route::resource('/user','Admin\UserController');
    Route::resource('/admins','Admin\AdminController');
    Route::resource('/categories','Admin\CategoryController');
    Route::resource('/settings','Admin\SettingsController');
    Route::resource('/reservation','Admin\ReservationsController');
    Route::resource('/reports','Admin\ReportsController');

    // halls owner
    Route::get('/hallsOwner','Admin\UserController@getHallsOwners');
    Route::get('/','Admin\IndexController@getIndex');

  });
});
