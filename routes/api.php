<?php

use Illuminate\Http\Request;

Route::post('login', 'API\AuthController@login');
Route::post('register', 'API\AuthController@register');

Route::group(['prefix' => '{token}/'], function () {
  Route::group(['middleware' => ['api_auth']], function () {
    Route::get('user/resend/active', 'API\AuthController@resend_active');
    Route::post('user/active', 'API\AuthController@active');
    Route::post('user', 'API\AuthController@update');
    Route::get('user', 'API\AuthController@user');
    Route::resource('notification', 'API\NotificationController');
    Route::resource('service', 'API\ServiceController');
    Route::resource('favourite', 'API\FavouriteController');
    Route::get('category/{id}/hall', 'API\HallController@category');
    Route::get('search/hall/{name}', 'API\HallController@search');
    Route::resource('hall', 'API\HallController');
    Route::resource('photo', 'API\PhotoController');
    Route::resource('report', 'API\ReportController');
    Route::resource('owner/hall', 'API\HallOwnerController');
    Route::post('owner/hall/{hall}/request/{id}', 'API\HallOwnerController@update');
    Route::get('owner/hall/{hall}/wait', 'API\HallOwnerController@show_waiting');
    Route::get('owner/hall/{hall}/accept', 'API\HallOwnerController@show_accepted');
    Route::get('owner/hall/{hall}/request/{id}', 'API\HallOwnerController@show');
    Route::get('reserver/hall/request/accept', 'API\HallReserverController@accept');
    Route::post('reserver/hall/request/{request}', 'API\HallReserverController@update');
    Route::resource('reserver/hall/request', 'API\HallReserverController');
    Route::get('logout', 'API\AuthController@logout');
  });
});
