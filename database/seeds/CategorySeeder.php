<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('categories')->insert(['name' => 'قاعات']);
      DB::table('categories')->insert(['name' => 'شاليهات']);
      DB::table('categories')->insert(['name' => 'استراحات']);
      DB::table('categories')->insert(['name' => 'حفلات اطفال']);
      DB::table('categories')->insert(['name' => 'تنسيق حفلات']);
      DB::table('categories')->insert(['name' => 'تنسيق وتغليف هدايا']);
      DB::table('categories')->insert(['name' => 'حلويات وضيافة']);
      DB::table('categories')->insert(['name' => 'اخرى']);
    }
}
