$(function () {

    'use strict';


    //----- OPEN model
    $('[data-popup-open]').on('click', function (e) {
        var targeted_popup_class = $(this).attr('data-popup-open'),
            url = $(this).attr('data-url');

        $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
        $('.confirm_deletion-form').attr('action', url);
        e.preventDefault();
    });
    //===========================================

    //----- CLOSE model
    $('[data-popup-close]').on('click', function (e) {
        var targeted_popup_class = $(this).attr('data-popup-close');
        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);

        e.preventDefault();
    });

    //--------------------------------


    $('.btn-close').on('click', function (e) {
        $('.modal-wrapper').addClass('modal-close');
    });






});
