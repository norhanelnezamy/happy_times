
@extends('admin.layout')
@section('content')
   @if(Session::has('message'))
          <p class="alert alert-info"><i class="fa fa-check"></i>{{ Session::get('message') }}</p>
   @endif
  <div class="col-xs-12 colmn_card">
    <div class="card card_ white lighten-2">
      <div class="card-header">
        <h5> التقريرات </h5>
      </div>
      <div class="card_body">
        <table id="example" class="cell-border display responsive-table" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>#</th>
              <th>اسم القاعة</th>
              <th>التقرير </th>
              <th>اسم العميل </th>
              <th> حذف </th>
            </tr>
          </thead>
          <tbody>
              <?php $i=1; ?>
            @foreach ($reports as $report)
              <tr>
                <td>{{ $i++ }}</td>
                <td>{{ $report->hall->name}}</td>
                <td>{{ $report->content }}</td>
                <td>{{ $report->user->name}}</td>
                <td>
                <button class="fa fa-remove remov_btn" data-popup-open="popup-1" data-url="{{ url('admin/reports/'. $report->id ) }}" title="مسح"></button>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

<div class="popup" data-popup="popup-1">
    <div class="popup-inner">
        <p> تأكيد حذف التقرير ؟ </p>
        <form class="model-form confirm_deletion-form" action="" method="post">
                 {{ csrf_field() }}
                 {{ method_field("DELETE") }}
            <input name="" class="btn btn-primary submit-name" type="submit" value="حذف">
            <a name="" class="btn btn-primary submit-name"data-popup-close="popup-1">تراجع </a>
        </form>
        <a class="popup-close" data-popup-close="popup-1" href="#">x</a>
    </div>
</div>
@endsection
