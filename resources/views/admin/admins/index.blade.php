
@extends('admin.layout')
@section('content')

  <div class="col-xs-12 colmn_card">
    <div class="card card_ white lighten-2">
      <div class="card-header">  
        <h5> اعضاء الأدارة</h5>
      </div>
      <div class="card_body">
        <table id="example" class="cell-border display responsive-table" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>#</th>
              <th>اسم العضو </th>
              <th>التحكم </th>
            </tr>
          </thead>
          <tbody>
              <?php $i=1; ?>
            @foreach ($admins as $admin)
              <tr>
                <td>{{ $i++ }}</td>
                <td>{{ $admin->name}}</td>
                <td>
                  <button class="fa fa-remove remov_btn" data-popup-open="popup-1" data-url="{{ url('admin/admins/'. $admin->id ) }}" title="مسح"></button>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

<div class="popup" data-popup="popup-1">
    <div class="popup-inner">
        <p>تأكيد الحذف ؟ </p>
        <form class="model-form confirm_deletion-form" action="" method="post">
                 {{ csrf_field() }}
                 {{ method_field("DELETE") }}
            <input name="" class="btn btn-primary submit-name" type="submit" value="حذف">
        <a class="btn btn-primary submit-name"data-popup-close="popup-1">تراجع</a>
        </form>
        <a class="popup-close" data-popup-close="popup-1" href="#">x</a>
    </div>
</div>
@endsection
