@extends('admin.layout')
  @section('content')
        <div class=" col-xs-12 colmn_card">
          <div class="card card_ card_form white lighten-2">
            <div class="card-header">
              <h5>اضافة عضو للأدارة</h5>
            </div>
            <div class="card_body">
              <div class="row">
                <!-- form start -->
                <form class="col s12" action="{{asset('admin/admins')}}" method="post">
                  {{ csrf_field() }}
                  <div class="row">
                    <div class="input-field col m12 s12">
                      <label for="name">الاسم </label>
                      <input value="{{old('name')}}" name="name"  id="name" type="text" class="validate">
                      @if ($errors->has('name'))
                        <p style="color:red">{{ $errors->first('name') }}</p>
                      @endif
                    </div>
                  </div> 
                  <div class="row">
                    <div class="input-field col m12 s12">
                      <label for="password">كلمة المرور </label>
                      <input value="{{old('password')}}" name="password"  id="password" type="text" class="validate">
                      @if ($errors->has('password'))
                        <p style="color:red">{{ $errors->first('password') }}</p>
                      @endif
                    </div>
                  </div> 
                    <div class="row">
                    <div class="input-field col m12 s12">
                      <label for="password">تأكيد كلمة المرور </label>
                      <input value="{{old('password')}}" name="password_confirmation"  id="password_confirmation" type="text" class="validate">
                      @if ($errors->has('password_confirmation'))
                        <p style="color:red">{{ $errors->first('password_confirmation') }}</p>
                      @endif
                    </div>
                  </div>
                  <button class="btn btn_submit" type="submit" name="action">حفظ
                    <i class="icon-save right"></i>
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div><!-- COLMN FOR FIRST CARD END-->
  @endsection
