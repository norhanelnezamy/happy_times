
@extends('admin.layout')
@section('content')
  <div class="col-xs-12 colmn_card">
    <div class="card card_ white lighten-2">
      <div class="card-header">
        <h5> الحجوزات</h5>
      </div>
      <div class="card_body">
        <table id="example" class="cell-border display responsive-table" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>#</th>
              <th>اسم القاعة</th>
              <th>الخدمات </th>
              <th>اسم العميل </th>
              <th> تاريخ الحجز </th>
            </tr>
          </thead>
          <tbody>
              <?php $i=1; ?>
            @foreach ($reservations as $reservation)
              <tr>
                <td>{{ $i++ }}</td>
                <td>{{ $reservation->hall->name}}</td>
                <td>
                    @foreach($reservation->Services() as $key => $serv)
                    { {{ $serv }} }
                    @endforeach
                </td>
                <td>{{ $reservation->user->name }}</td>
                <td>{{ $reservation->date }}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection
