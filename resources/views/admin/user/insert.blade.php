@extends('admin.layout')
  @section('content')
        <div class=" col-xs-12 colmn_card">
          <div class="card card_ card_form white lighten-2">
            <div class="card-header">
              <h5>اضافة مستخدم</h5>
            </div>
            <div class="card_body">
              <div class="row">
                <!-- form start -->
                <form class="col s12" action="{{asset('admin/user')}}" method="post">
                  {{ csrf_field() }}
                  <div class="row">
                    <div class="input-field col m6 s12">
                      <label for="name">الاسم </label>
                      <input value="{{old('name')}}" name="name"  id="first_name" type="text" class="validate">
                      @if ($errors->has('name'))
                        <p style="color:red">{{ $errors->first('name') }}</p>
                      @endif
                    </div>
                    <div class="input-field col m6 s12">
                      <label for="phone">رقم الجوال </label>
                      <input value="{{old('phone')}}" name="phone" id="last_name" type="text" class="validate">
                      @if ($errors->has('phone'))
                        <p style="color:red">{{ $errors->first('phone') }}</p>
                      @endif
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col m6 s12">
                      <label for="password">كلمة المرور </label>
                      <input value="{{old('password')}}" name="password" type="password"  id="first_name" type="text" class="validate">
                      @if ($errors->has('password'))
                        <p style="color:red">{{ $errors->first('password') }}</p>
                      @endif
                    </div>
                    <div class="input-field col m6 s12">
                      <label for="password_confirmation">تأكيد كلمة المرور </label>
                      <input value="{{old('password_confirmation')}}" name="password_confirmation" type="password" id="last_name" type="text" class="validate">
                      @if ($errors->has('password_confirmation'))
                        <p style="color:red">{{ $errors->first('password_confirmation') }}</p>
                      @endif
                    </div>
                  </div>
                
                  <div class="row">
                    <div class="input-field col s12">
                      <p class="radio_p">مقدم خدمات
                        <input name="is_hall" type="radio" id="test1" value="1" />
                        <label for="test1"></label>
                      </p>
                      <p class="radio_p">مستخدم عادى
                        <input name="is_hall" type="radio" id="test2" value="0"/>
                        <label for="test2"></label>
                      </p>
                      @if ($errors->has('is_hall'))
                        <p style="color:red">{{ $errors->first('is_hall') }}</p>
                      @endif
                    </div>
                  </div>
                  <button class="btn btn_submit" type="submit" name="action">تسجيل
                    <i class="material-icons right">send</i>
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div><!-- COLMN FOR FIRST CARD END-->
  @endsection
