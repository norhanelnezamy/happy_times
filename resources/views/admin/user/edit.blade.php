@extends('admin.layout')
  @section('header_p')
    <small>المستخدمين  </small>
  @endsection
  @section('header_li')
    <li>تعديل بيانات المستخدم   </li>
  @endsection
  @section('content')
    <!-- Horizontal Form -->
      <div class="box box-info">
        <div class="box-header with-border">
          <p class="box-title">تعديل بيانات المستخدم </p>
        </div><!-- /.box-header -->
        <!-- form start -->
          
           @if(Session::has('message'))
                  <p class="alert alert-info"><i class="fa fa-check"></i>{{ Session::get('message') }}</p>
           @endif
        <form class="form-horizontal  mainform" action="{{asset('admin/user/'.$user->id)}}" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          {{ method_field('PUT') }}
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-2 control-label">اسم المستخدم </label>
              <div class="col-sm-12">
                <input value="{{old('name', $user->name)}}" name="name" type="text" class="form-control">
                @if ($errors->has('name'))
                  <p style="color:red">{{ $errors->first('name') }}</p>
                @endif
              </div>
            </div>
          </div>
          <!-- /.box-body -->  
            <div class="box-body">
            <div class="form-group">
              <label class="col-sm-2 control-label">رقم الجوال  </label>
              <div class="col-sm-12">
                <input value="{{old('phone', $user->phone)}}" name="phone" type="text" class="form-control">
                @if ($errors->has('phone'))
                  <p style="color:red">{{ $errors->first('phone') }}</p>
                @endif
              </div>
            </div>
          </div>
          <!-- /.box-body -->
            
        
            
         <div class="row">
                <div class="input-field col s12">
                  <p class="radio_p">مقدم خدمات
                    <input name="is_hall"<?php echo ($user->is_hall == 1) ? 'checked' : '' ?> type="radio" id="test1" value="1" />
                    <label for="test1"></label>
                  </p>
                  <p class="radio_p">مستخدم عادى
                    <input name="is_hall" <?php echo ($user->is_hall == 0) ? 'checked' : '' ?> type="radio" id="test2" value="0"/>
                    <label for="test2"></label>
                  </p>
                  @if ($errors->has('is_hall'))
                    <p style="color:red">{{ $errors->first('is_hall') }}</p>
                  @endif
                </div>
         </div>
            <input type="hidden" value="{{$user->id }}" name="user_id">
            
          <div class="box-footer">
            <button type="submit" class="btn btn-info col-md-2" style="float:left;margin-left:35%;" > حفظ </button>
          </div><!-- /.box-footer -->
        </form>
      </div>
      <!-- /.box -->

      <script src="{{ asset('admin_asset/plugins/ms-dropdown-plugin/js/msdropdown/jquery-1.3.2.min.jss')}}"></script>
      <script src="{{ asset('admin_asset/plugins/ms-dropdown-plugin/js/msdropdown/jquery.dd.min.js')}}"></script>
      <!-- Page script -->
  @endsection
