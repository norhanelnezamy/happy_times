
@extends('admin.layout')
@section('content')
 @if(Session::has('message'))
     <p class="alert alert-info"><i class="fa fa-check"></i>{{ Session::get('message') }}</p>
  @endif
  <div class="col-xs-12 colmn_card">
    <div class="card card_ white lighten-2">
      <div class="card-header">
        <h5> المستخدمين</h5>
      </div>
      <div class="card_body">
        <table id="example" class="cell-border display responsive-table" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>#</th>
              <th>الاسم</th>
              <th>الجوال </th>
              <th>تاريخ التسجيل </th>
              <th>التحكم </th>
            </tr>
          </thead>
          <tbody>
            @foreach ($users as $key => $user)
              <tr>
                <td>{{ $user->id }}</td>
                <td>{{ $user->name}}</td>
                <td>{{ $user->phone }}</td>
                <td>{{ $user->created_at }}</td>
                <td>
                  <form action="{{ url('admin/user/'. $user->id.'/edit') }}" method="get" style="Display:inline-block;">
                    <button class="fa fa-pencil edit_btn" title="تعديل"></button>
                  </form>
                  <button class="fa fa-remove remov_btn" data-popup-open="popup-1" data-url="{{ url('admin/user/'. $user->id ) }}" title="مسح"></button>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

<div class="popup" data-popup="popup-1">
    <div class="popup-inner">

       <p>تأكيد الحذف ؟ </p>
        <form class="model-form confirm_deletion-form" action="" method="post">
                 {{ csrf_field() }}
                 {{ method_field("DELETE") }}
            <input name="" class="btn btn-primary submit-name" type="submit" value="حذف">
            <a class="btn btn-primary submit-name"data-popup-close="popup-1">تراجع </a>
        </form>
        <a class="popup-close" data-popup-close="popup-1" href="#">x</a>
    </div>
</div>
@endsection
