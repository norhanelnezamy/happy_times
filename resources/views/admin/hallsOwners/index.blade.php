
@extends('admin.layout')
@section('content')
  <div class="col-xs-12 colmn_card">
    <div class="card card_ white lighten-2">
      <div class="card-header">
          
               @if(Session::has('message'))
                  <p class="alert alert-info"><i class="fa fa-check"></i>{{ Session::get('message') }}</p>
                @endif
          
        <h5> مقدمين الخدمات</h5>
      </div>
      <div class="card_body">
        <table id="example" class="cell-border display responsive-table" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>#</th>
              <th>الأسم </th>
              <th>اسم الخدمة </th>
              <th>الجوال  </th>
              <th>التحكم </th>
            </tr>
          </thead>
          <tbody>
              <?php $i=1; ?>
            @foreach ($hallsOwners as $owner)
              <tr>
                <td>{{ $i++ }}</td>
                <td>{{ $owner->name}}</td>
                <td> 
                    @foreach($owner->halls as $hall)
                    {{ $hall->name .'' }}</td>
                    @endforeach
                <td>{{ $owner->phone }}</td>
                <td>
                  <form action="{{ url('admin/user/'. $owner->id.'/edit') }}" method="get" style="Display:inline-block;">
                    <button class="fa fa-pencil edit_btn" title="تعديل"></button>
                  </form>
                <button class="fa fa-remove remov_btn" data-popup-open="popup-1" data-url="{{ url('admin/user/'. $owner->id ) }}" title="مسح"></button>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

<div class="popup" data-popup="popup-1">
    <div class="popup-inner">
       <p>تـأكـيــد الحـذف ؟ </p>
        <form class="model-form confirm_deletion-form" action="" method="post">
                 {{ csrf_field() }}
                 {{ method_field("DELETE") }}
            <input name="" class="btn btn-primary submit-name" type="submit" value="حذف">
            <a class="btn btn-primary submit-name"data-popup-close="popup-1">تراجع </a>
        </form>
        <a class="popup-close" data-popup-close="popup-1" href="#">x</a>
    </div>
</div>
@endsection
