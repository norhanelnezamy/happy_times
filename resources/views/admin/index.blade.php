@extends('admin.layout')
   @section('content')
     <!-- statistics here -->
     <div class="col-md-3 col-sm-6 col-xs-12">
       <div class="card statistics_3">
         <div class="">
           <strong class="text-xl">{{ $requests }}<i class="material-icons">insert_invitation</i></strong><br/>
           <span class="opacity-50">طلبات الحجز </span>
         </div>
       </div>
     </div>
     <div class="col-md-3 col-sm-6 col-xs-12">
       <div class="card statistics_3">
         <div class="">
           <strong class="text-xl">{{ $reports }}<i class="material-icons">flag</i></strong><br/>
           <span class="opacity-50">الابلاغات</span>
         </div>
       </div>
     </div>
     <div class="col-md-3 col-sm-6 col-xs-12">
       <div class="card statistics_3">
         <div class="">
           <strong class="text-xl">{{ $halls }} <i class="material-icons">card_giftcard</i> </strong><br/>
           <span class="opacity-50">  مقدمى الخدمات</span>
         </div>
       </div>
     </div>
     <div class="col-md-3 col-sm-6 col-xs-12">
       <div class="card statistics_3">
         <div class="">
           <strong class="text-xl">{{ $users }} <i class="material-icons">account_circle</i> </strong><br/>
           <span class="opacity-50">المستخدمين</span>
         </div>
       </div>
     </div>
     <!-- statistics end -->

     <div class="col-xs-12">
       <div class="card card_  white lighten-2">
         <div class="card-header">
           <h5>خريطه جوجل  </h5>
         </div>
         <div class="card_body">
           <div class="map-responsive">
          <div id="map"  class="map_"></div>
           </div>
         </div>
       </div>
     </div>

     <script>
  function initMap() {
    var zajan = {lat: 24.7136, lng: 46.6753};

    var map = new google.maps.Map(document.getElementById('map'), {
      scaleControl: true,
      center: zajan,
      zoom: 8
    });

    var infowindow = new google.maps.InfoWindow;
    infowindow.setContent('<b>السعودية</b>');

    var marker = new google.maps.Marker({map: map, position: zajan});
    marker.addListener('click', function() {
      infowindow.open(map, marker);
    });
  }
</script>
<script async defer src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBGERiio3BH9dTWcAghYLpldQKnWFr9-OE&callback=initMap"></script>
   @endsection
