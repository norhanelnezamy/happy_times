<!DOCTYPE html>
  <html>
    <head>
      <!--Let browser know website is optimized for mobile-->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('admin_asset/bootstrap/css/bootstrap.css')}}">
             <!--font Awesome-->
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
      <!--Import Google Icon Font-->

      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="{{asset('admin_asset/css/materialize.min.css')}}"  media="screen,projection"/>
      <link rel="stylesheet" href="{{asset('admin_asset/bootstrap/css/stylesheet.css')}}">
      <link rel="stylesheet" href="{{asset('admin_asset/bootstrap/css/responsive.css')}}">

        </head>

    <body>



        <div class="container-fluid">
          <li class="home_ waves-effect waves-light"> <a href="{{ asset('admin')}}"class="home waves-effect waves-light"> الرئيسيه <i class="material-icons">home</i></a></li>

          <div class="page_404">
            <h1>404 <i class="material-icons">search</i></h1>
            <p>هذا المحتوى غير موجود </p>
          </div>
         </div>

      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="{{asset('admin_asset/bootstrap/js/jquery-3.1.1.min.js')}}" ></script>
      <script type="text/javascript" src="{{asset('admin_asset/bootstrap/js/bootstrap.min.js')}}" ></script>
      <script type="text/javascript" src="{{asset('admin_asset/js/materialize.min.js')}}"></script>

      <script type="text/javascript" src="{{asset('admin_asset/bootstrap/js/javascript.js')}}" ></script>
      <script type="text/javascript">
      </script>
    </body>
  </html>
