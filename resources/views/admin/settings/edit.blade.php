@extends('admin.layout')
  @section('content')
    <div class=" col-xs-12 colmn_card">
      <div class="card card_ card_form white lighten-2">
        <div class="card-header">
          <h5>تعديل بياناتك</h5>
        </div>
        <div class="card_body">
          <div class="row">
          @if(Session::has('error'))
                  <p class="alert alert-warning"><i class="fa fa-check"></i>{{ Session::get('error') }}</p>
           @endif
        <form class="form-horizontal  mainform" action="{{asset('admin/settings/'.$admin->id)}}" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          {{ method_field('PUT') }}
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-2 control-label">الاسم  </label>
              <div class="col-sm-12">
                <input value="{{old('name', $admin->name)}}" name="name" type="text" class="form-control">
                @if ($errors->has('name'))
                  <p style="color:red">{{ $errors->first('name') }}</p>
                @endif
              </div>
            </div>
          </div>
            <div class="box-body">
            <div class="form-group">
              <label class="col-sm-2 control-label">كلمة المرور  </label>
              <div class="col-sm-12">
                <input  name="password" type="text" class="form-control">
                @if ($errors->has('password'))
                  <p style="color:red">{{ $errors->first('password') }}</p>
                @endif
              </div>
            </div>
          </div>
            <div class="box-body">
            <div class="form-group">
              <label class="col-sm-2 control-label">  كلمة المرور القديمة  </label>
              <div class="col-sm-12">
                <input  name="old_password" type="text" class="form-control">
                @if ($errors->has('old_password'))
                  <p style="color:red">{{ $errors->first('old_password') }}</p>
                @endif
              </div>
            </div>
          </div>
            <input type="hidden" name="admin_id" value="{{$admin->id}}">
            <button class="btn btn_submit" type="submit" name="action">حفظ
              <i class="icon-save right"></i>
            </button>
        </form>
      </div>
    </div>
  </div>
</div>
      <!-- /.box -->
  @endsection
