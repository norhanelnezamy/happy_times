@extends('admin.layout')
  @section('content')
        <div class=" col-xs-12 colmn_card">
          <div class="card card_ card_form white lighten-2">
            <div class="card-header">
              <h5>تعديل فئة </h5>
            </div>
            <div class="card_body">
              <div class="row">
                <!-- form start -->
                <form class="col s12" action="{{asset('admin/categories/').$data['category']['id']}}" method="post">
                  {{ csrf_field() }}
                  <div class="row">
                    <div class="input-field col m12 s12">
                      <label for="name">الاسم </label>
                      <input value="{{old('name',$data['category']['name'])}}" name="name"  id="name" type="text" class="validate">
                      @if ($errors->has('name'))
                        <p style="color:red">{{ $errors->first('name') }}</p>
                      @endif
                    </div>
                  </div>
                  <button class="btn btn_submit" type="submit" name="action">حفظ
                    <i class="icon-save right"></i>
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div><!-- COLMN FOR FIRST CARD END-->
  @endsection
