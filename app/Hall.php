<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Photo;

class Hall extends Model
{
    public static function add($request){
      $hall = new Hall;
      $hall->name = $request->name;
      $hall->address = $request->address;
      $hall->phone = $request->phone;
      $hall->additional = $request->additional;
      $hall->main_photo = mob_upload_photo($request->main_photo);
      $hall->user_id = Auth::user()->id;
      $hall->cat_id = $request->cat_id;
      $hall->save();
      Photo::add($request->photos, $hall->id);
    }

    public static function edit($request, $id){
      $hall = Hall::findOrFail($id);
      $hall->name = $request->name;
      $hall->address = $request->address;
      $hall->phone = $request->phone;
      $hall->additional = $request->additional;
      $hall->save();
    }

    public function favourites()
    {
        return $this->belongsToMany('App\Favourite');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public function cateory()
    {
        return $this->belongsTo('App\Models\Category');
    }
    
    public function reservation()
    {
        return $this->hasMany('App\HallRequest');
    }
}

