<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    public static function add($photos, $hall_id){
      foreach ($photos as $key => $ph) {
        $photo = new Photo;
        $photo->url = mob_upload_photo($ph);
        $photo->hall_id = $hall_id;
        $photo->save();
      }
    }
}
