<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
  public static function add($request){
    $feedback = new Feedback;
    $feedback->writer_id = Auth::user()->id ;
    $feedback->user_id = $request->user_id ;
    $feedback->content = $request->content ;
    $feedback->save();
  }

  public static function edit($request, $id){
    $feedback = Feedback::findOrFail($id);
    $feedback->content = $request->content ;
    $feedback->save();
  }
}
