<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Service;

class HallRequest extends Model
{
    protected $table = 'requests';

    public static function add($request){
      $hall_request = new HallRequest;
      $hall_request->user_id = Auth::user()->id;
      $hall_request->hall_id = $request->hall_id;
      $hall_request->date = $request->date;
      $hall_request->time = $request->time;
      $hall_request->services = json_encode($request->services);
      $hall_request->save();
      return $hall_request;
    }

    public static function edit($request, $id){
      $hall_request = HallRequest::findOrFail($id);
      $hall_request->response = $request->status;
      $hall_request->save();
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function hall()
    {
        return $this->belongsTo('App\Hall');
    }


    public function Services()
    {

      $arrayOfServs = json_decode($this->services);
      $arr = [];
      foreach($arrayOfServs as $d)
      {
          $arr[] = Service::find($d)->name;
      }
      return $arr;


    }
}
