<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mobile_Token extends Model
{
    public static function check($request, $user_id){
      if (sizeof(Mobile_Token::where('user_id', $user_id)->where('token', $request->mobile_type)->first()) == 0) {
        $mobile = new Mobile_Token;
        $mobile->mobile_type = $request->mobile_type;
        $mobile->token = $request->mobile_token;
        $mobile->user_id = $user_id;
        $mobile->save();
      }
    }
}
