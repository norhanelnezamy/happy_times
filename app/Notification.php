<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
  public static function add($request){
    $notification = new Notification;
    $notification->user_id = $request['user_id']['id'];
    $notification->request_id = $request['request_id'];
    $notification->content = $request['content'];
    $notification->save();
    notifi($notification);
  }

  public static function edit($id){
    $notification = Notification::findOrFail($id);
    $notification->seen = 1;
    $notification->save();
  }
}
