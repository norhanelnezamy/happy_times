<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public static function add($request){
      $service = new Service;
      $service->name = $request->name ;
      $service->price = $request->price ;
      $service->hall_id = $request->hall_id ;
      $service->save();
    }
}
