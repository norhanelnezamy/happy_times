<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use PushNotification;
use Auth;
use App\Hall;
use App\Photo;
use App\Models\Category;
use App\Service;

class HallController extends Controller
{
    public function __construct()
    {
        $this->middleware('hall_owner', ['only' => ['delete', 'update']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // PushNotification::app('appNameAndroid')->to('flG84CWu6wI:APA91bFC3U5g4mGcdaHC7krBXp9GHsDSh-q6LPD94al3sVdGpmCQjN1bgkUOkbA29KUFWpFtblPUOgG3JGvVTqgTcSQuAIQWMHgqUrgUEUwvzstVerMGJ23niZQ9-aSTWqpCyMWfCy_C')->send("meh :(");
        $halls = Hall::leftJoin('favourites', 'halls.id', 'favourites.hall_id')->select('halls.*', 'favourites.id as favourite')->paginate(10);
        return response($halls, 200);
    }

    public function search($token, $name)
    {
        $halls = Hall::leftJoin('favourites', 'halls.id', 'favourites.hall_id')->where('halls.name', 'like', '%'.$name.'%')->select('halls.*', 'favourites.id as favourite')->paginate(10);
        return response($halls, 200);
    }

    public function category($token ,$id)
    {
        $halls = Hall::leftJoin('favourites', 'halls.id', 'favourites.hall_id')->where('halls.cat_id', $id)->select('halls.*', 'favourites.id as favourite')->paginate(10);
        return response($halls, 200);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return response($categories, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Hall::add($request);
        return response()->json(['status' => 1], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($token , $id)
    {
        $hall['data'] = Hall::findOrFail($id);
        $hall['photos'] = Photo::where('hall_id', $id)->get();
        $hall['services'] = Service::where('hall_id', $id)->get();
        return response($hall, 200);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$token, $id)
    {
        Hall::edit($request, $id);
        return response()->json(['status' => 1], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($token, $id)
    {
        Hall::findOrFail($id)->delete();
        return response()->json(['status' => 1], 200);
    }
}
