<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\HallRequest;
use App\Notification;
use PushNotification;
use Auth;
use App\Hall;
use App\Service;

class HallOwnerController extends Controller
{

    public function __construct()
    {
        $this->middleware('hall_owner')->except('index');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $halls = Hall::where('user_id', Auth::user()->id)->paginate(10);
        return response($halls, 200);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($token, $hall_id , $request_id)
    {
        $request = HallRequest::join('halls', 'requests.hall_id', 'halls.id')->join('users', 'requests.user_id', 'users.id')->where('requests.id', $request_id)->select('requests.*','halls.name as hall_name', 'users.name', 'users.phone')->first();
        $services_id = json_decode($request->services, true);
        $request['services'] = Service::whereIn('id', $services_id)->get();
        return response($request, 200);
    }

    public function show_waiting($token, $id)
    {
      $requests = HallRequest::join('users', 'requests.user_id', 'users.id')->where('requests.response', 0)->where('requests.hall_id', $id)->select('requests.*', 'users.name', 'users.phone')->paginate(15);
      return response($requests, 200);
    }

    public function show_accepted($token, $id)
    {
      $requests = HallRequest::join('users', 'requests.user_id', 'users.id')->where('requests.response', 1)->where('requests.hall_id', $id)->select('requests.*', 'users.name', 'users.phone')->paginate(15);
      return response($requests, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $token, $hall ,$id)
    {
        HallRequest::edit($request, $id);
        $notification['request_id'] = $id;
        $notification['user_id'] = HallRequest::join('halls', 'requests.hall_id', 'halls.id')->join('users', 'halls.user_id', 'users.id')->where('requests.id', $id)->select('users.id')->first();
        if ($request->status == 1) {
          $notification['content'] = "تم قبول طلب الحجز ";
        }
        if ($request->status == 2) {
          $notification['content'] = "تم رفض طلب الحجز ";
        }
        Notification::add($notification);
        return response()->json(['status' => 1], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($token, $id)
    {
        //
    }
}
