<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Auth;
use App\Notification;
use App\HallRequest;
use App\Hall;
use App\Service;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notifications = Notification::where('user_id', Auth::user()->id)->paginate(10);
        return response($notifications, 200);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($token, $id)
    {
        $notification['data'] = Notification::findOrFail($id);
        $notification['request'] = HallRequest::findOrFail($notification['data']['request_id']);
        $notification['hall'] = Hall::findOrFail($notification['request']['hall_id']);
        $services_id = json_decode($notification['request']['services'], true);
        $notification['request_services'] = Service::whereIn('id', $services_id)->get();
        return response($notification, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $token, $id)
    {
        Notification::edit($id);
        return response()->json(['status' => 1]);
    }

}
