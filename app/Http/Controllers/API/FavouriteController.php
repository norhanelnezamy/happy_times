<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Auth;
use App\Favourite;
use App\Ad;

class FavouriteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $favourites = Favourite::join('halls', 'favourites.hall_id', 'halls.id')->where('favourites.user_id', Auth::user()->id)->select('favourites.id as favourite', 'halls.*')->paginate(10);
      return response($favourites, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Favourite::add($request);
        return response()->json(['status' => 1], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($token, $id)
    {
        Favourite::findOrFail($id)->delete();
        return response()->json(['status' => 1], 200);
    }
}
