<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\HallRequest;
use App\Notification;
use Auth;
use App\Service;

class HallReserverController extends Controller
{

    public function __construct()
    {
        $this->middleware('hall_reserver', ['only' => ['update', 'show']]);
    }

    public function index()
    {
        $requests = HallRequest::join('halls', 'requests.hall_id', 'halls.id')->where('requests.user_id', Auth::user()->id)->where('requests.response', 0)->select('requests.*', 'halls.name')->paginate(15);
        return response($requests, 200);
    }

    public function accept($token)
    {
        $requests = HallRequest::join('halls', 'requests.hall_id', 'halls.id')->where('requests.user_id', Auth::user()->id)->where('requests.response', 1)->select('requests.*', 'halls.name')->paginate(15);
        return response($requests, 200);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hall_request = HallRequest::add($request);
        $notification['request_id'] = $hall_request->id;
        $notification['user_id'] = HallRequest::join('halls', 'requests.hall_id', 'halls.id')->join('users', 'halls.user_id', 'users.id')->where('requests.id', $hall_request->id)->select('users.id')->first();
        $notification['content'] = "طلب حجز جديد ";
        Notification::add($notification);
        return response()->json(['status' => 1], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($token, $id)
    {
        $request = HallRequest::join('halls', 'requests.hall_id', 'halls.id')->where('requests.id', $id)->first();
        $services_id = json_decode($request->services, true);
        $request['services'] = Service::whereIn('id', $services_id)->get();
        return response($request, 200);
    }

    public function update(Request $request, $token ,$id)
    {
        HallRequest::edit($request, $id);
        $notification['request_id'] = $id;
        $notification['user_id'] = HallRequest::join('halls', 'requests.hall_id', 'halls.id')->join('users', 'halls.user_id', 'users.id')->where('requests.id', $id)->select('users.id')->first();
        $notification['content'] = "تم الغاء طلب حجز ";
        Notification::add($notification);
        return response()->json(['status' => 1], 200);
    }

}
