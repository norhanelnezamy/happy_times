<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\User;
use Auth;
use App\Mobile_Token;

class AuthController extends Controller
{
  public function login(Request $request){
    if (Auth::attempt([ 'phone' => $request->phone , 'password' => $request->password ])) {
      Mobile_Token::check($request, Auth::user()->id);
      return response(Auth::user(), 200);
    }
    $response['status'] = 0;
    return response($response,200);
  }

  public function register(Request $request){
    if (sizeof(User::where('phone', $request->phone)->get()) > 0) {
      return response()->json(['status' => 0], 200);
    }
    $user = User::add($request);
    Mobile_Token::check($request, $user->id);
    return response($user,200);
  }

  public function resend_active($token){
    User::resend_active();
    return response()->json(['status' => 1], 200);
  }

  public function active(Request $request){
    $response['status'] = User::active($request);;
    return response($response,200);
  }

  public function update(Request $request, $token){
    User::edit($request);
    return response()->json(['status' => 1], 200);
  }

  public function user(){
    return response(Auth::user(), 200);
  }

  public function logout(){
    Auth::logout();
    return response()->json(['status' => 1], 200);
  }
}
