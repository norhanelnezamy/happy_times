<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Auth;
use App\Hall;
use App\Service;

class ServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('hall_owner', ['only' => ['delete', 'store']]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Service::add($request);
        return response()->json(['status' => 1], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($token, $id)
    {
        $services = Service::where('hall_id', $id)->get();
        return response($services, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($token, $id)
    {
        Service::findOrFail($id)->delete();
        return response()->json(['status' => 1], 200);
    }
}
