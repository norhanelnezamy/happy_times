<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Http\Requests\UserRequest;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $icon ='user';
      $users = User::all();
      return view('admin.user.index', compact('users', 'icon'));
    }


    
    public function create()
    {
      $icon ='user';
      return view('admin.user.insert', compact('icon'));
    }


    
    public function store(UserRequest $request)
    {
      User::add($request);
      $icon ='user';
      return redirect('admin/user')->with('message', 'تم اضافه مستخدم جديد .')->with('icon', $icon);
    }


    public function edit($id)
    {
      $icon ='user';
      $user = User::findOrFail($id);
      return view('admin.user.edit', compact('user', 'icon'));
        
        
    }


    public function update(UserRequest $request, $id)
    {
      User::where('id',$id)->update(['name'=>$request->name,'phone'=>$request->phone,'is_hall'=>$request->is_hall]);
      $icon ='user';
      return redirect()->back()->with('message', 'تم تعديل بيانات المستخدم .')->with('icon', $icon);
    }

   
    
    public function destroy($id)
    {
          $user = User::find($id);
         if($user->halls->count() != 0)
            foreach ($user->halls as $hall) 
                $hall->delete();
        
        
      User::findOrFail($id)->delete();
      $icon ='user';
      return redirect()->back()->with('message', 'تم مسح المستخدم .')->with('icon', $icon);
    }
    
    
    public function getHallsOwners()
    {
        $icon = 'reserve_owner';
        $hallsOwners = User::where('is_hall',1)->get();
        return view('admin.hallsOwners.index', compact('hallsOwners','icon'));
        
    }
}
