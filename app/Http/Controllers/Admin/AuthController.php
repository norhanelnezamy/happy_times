<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Http\Requests\LoginRequest;
use Auth;

class AuthController extends Controller
{
  /**
   * Display admin login view.
   *
   * @return \Illuminate\Http\Response
   */
    public function getLogin(){
      if (Auth::guard('admin')->user()) {
        return redirect('/admin');
      }
      return view('admin.login');
    }

    /**
     * authenticate admin .
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(LoginRequest $request){
      if (Auth::guard('admin')->attempt([ 'name' => $request->name , 'password' => $request->password ])) {
         return redirect('/admin');
      }
      return redirect()->back()->with('authorized', 'خطأ ف بيانات المستخدم .');
    }


    public function getLogout(){
      Auth::guard('admin')->logout();
      return redirect('admin/login');
    }
}
