<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\User;
use App\Report;
use App\HallRequest;
use App\Hall;

class IndexController extends Controller
{
  public function getIndex(){
    $users = User::count();
    $reports = Report::count();
    $requests = HallRequest::count();
    $halls = Hall::count();
    $icon = 'index';
    return view('admin.index', compact('users', 'reports', 'requests', 'halls', 'icon'));
  }

}
