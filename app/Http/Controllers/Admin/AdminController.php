<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\AdminRequest;
use Illuminate\Routing\Controller;
use App\Admin;
use Auth;

class AdminController extends Controller
{

  public function index()
  {
    $icon ='admin';
    $admins = Admin::paginate(15);
    return view('admin.admins.index', compact('admins','icon'));
  }


    
  public function create()
  {
    $icon ='admin';
    return view('admin.admins.create',compact('icon'));
  }

    
 
    
  public function store(AdminRequest $request)
  {
      Admin::add($request);
      return redirect('admin/admins')->with('message', 'تم اضافة عضو الى الادارة .');
  }



  public function destroy($id)
  {
    Admin::findOrFail($id)->delete();
    return redirect()->back()->with('message', 'تم مسح عضو من الادارة .');
  }
}
