<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Report;

class ReportsController extends Controller
{
    
    public function index()
    {
        $data['reports'] = Report::all();
        $data['icon'] = 'report';
        return view('admin.reports.index',$data);
    }
    
    
    public function destroy($id)
    {
        Report::destroy($id);
        return redirect('admin/reports')->with('message', 'تم حذف التقرير  .');
    }
}
