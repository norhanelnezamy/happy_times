<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
class CategoryController extends Controller
{
    
    
    
    
    public function index()
    {   $data['icon'] = 'cat';
        $data['categories'] = Category::all();
        return view('admin.categories.index',$data);
    }
    
    public function create()
    {
         $data['icon'] = 'cat';
        return view('admin.categories.create',$data);
    }
    
    
    
    public function store(CategoryRequest $request)
    {
      Category::create($request->all());
      return redirect('admin/categories')->with('message', 'تم أضافة فئة جديدة .');
    }
    
    
    public function edit($id)
    {
         $data['icon'] = 'cat';
        $data['category'] = Category::find($id);
        return view('admin.categories.edit',$data);
    }
    
    
    public function update(CategoryRequest $request,$id)
    {
        Category::where('id',$id)->update(['name'=>$request->name]);
        return redirect('admin/categories')->with('message', 'تم تعديل الفئة  .');
    }
    
    
    public function destroy($id)
    {
          $cat = Category::find($id);
         if($cat->halls->count() != 0)
            foreach ($cat->halls as $hall) 
                $hall->delete();
        
        Category::destroy($id);
        return redirect('admin/categories')->with('message', 'تم حذف الفئة  .');
    }
}
