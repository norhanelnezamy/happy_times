<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SettingsRequest;
use Auth;
use App\Admin;

class SettingsController extends Controller
{
    
    
 public function index()
  {
      $id = Auth::user()->id;
      $data['admin'] = Admin::findOrFail($id);
      $data['icon'] = 'setting';
      return view('admin.settings.edit', $data);
  }
    
  public function update(SettingsRequest $request, $id)
  {
//dd($request->old_password);
      if($request->has('old_password') and $request->old_password != '')
      {
          if(password_verify($request->old_password, Auth::user()->password))
          {
             Admin::edit($request , $id); 
          }
          else
          {
             return redirect()->back()->with('error','كلمة المرور القديمة غير صحيحة ');
          }
      }else
      {
        return redirect()->back()->with('error','ادخل كلمة المرور القديمة ');     
      }
      return redirect()->back()->with('message','تم تعديل بياناتك بنجاح');
  }
}
