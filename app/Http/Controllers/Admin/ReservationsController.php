<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\HallRequest;


class ReservationsController extends Controller
{
    
    
    
    public function index()
    {
        $data['icon'] = 'reserve';
        $data['reservations'] = HallRequest::all();
        return view('admin.reservations.index',$data);
    }
}
