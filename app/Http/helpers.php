<?php
use Davibennun\LaravelPushNotification\Facades\PushNotification as PushNotification;
use App\Notification;
use App\User;
use Illuminate\Support\Facades\Auth as Auth;
use App\Mobile_Token;

  function upload($file){
    $name = $file->getClientOriginalName() ;
    $fileName = date("Y_h:i:s_A").'_'.$name ;
    $file->move('uploads', $fileName) ;
    return $fileName ;
  }

  function mob_upload_photo($photo){
    $photo_name = date("Y_h:i:s_A").'_'.mt_rand().'.jpg';
    $decode = base64_decode($photo);
    $path =  getcwd().'/uploads/'.$photo_name;
    $file = file_put_contents($path, $decode);
    return $photo_name;
  }
  function notifi($not){
    $android = Mobile_Token::where('user_id', $not->user_id)->where('mobile_type', 'android')->get();
    $ios = Mobile_Token::where('user_id', $not->user_id)->where('mobile_type', 'ios')->get();
    foreach ($android as $key => $device) {
      PushNotification::app('appNameAndroid')->to($device->token)->send($not);
    }
    foreach ($ios as $key => $device) {
      PushNotification::app('appNameIOS')->to($device->token)->send($not);
    }
    // return $user_android_tokens;
  }

?>
