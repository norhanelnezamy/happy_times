<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
     {
       $rules = [
         'name' => 'required|max:60',
         'is_hall' => 'required',
//         'address' => 'required',
       ];
       if (request()->has('user_id')) {
         $rules['phone'] = 'required|numeric|unique:users,phone,'.request()->user_id;
         $rules['password'] = 'alpha_num|between:6,12|confirmed';
         $rules['password_confirmation'] = 'alpha_num|between:6,12|';
       }
       else {
         $rules['phone'] = 'required|numeric|unique:users,phone';
         $rules['password'] = 'required|alpha_num|between:6,12|confirmed';
         $rules['password_confirmation'] = 'required|alpha_num|between:6,12|';
       }
       return $rules;
     }
     public function messages(){
       return[
         'name.required' => 'يجب ادخال الاسم الاول .' ,
         'name.max' => 'يجب ادخال اسم لا يزيد عن 60 حرف .',
         'password.required' => 'يجب ادخال كلمه المرور .',
         'password.alpha_num' => 'غير مصرح بدخول اى علامات ,استخدم فقط الحروف والارقام .',
         'password.between' => 'يجب عدم ادخال كلمه مرور بين 6:12 حرف',
         'password.confirmed' => 'كلمتى المرور غير متطابقتين .',
         'password_confirmation.required' => 'يجب ادخال تأكيد كلمه المرور .',
         'address.required' => 'يجب ادخال العنوان التفصيلي .' ,
         'phone.required' => 'يجب ادخال رقم الجوال .' ,
         'phone.unique' => 'تم التسجيل برقم الهاتف من قبل .',
         'phone.numeric' => 'يجب ادخال رقم جوال صحيح .' ,
         'is_hall.required' => 'يجب اختيار نوع المسجل .' ,
       ];
     }
}
