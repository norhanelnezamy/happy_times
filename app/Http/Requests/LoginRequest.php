<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
     {
       if (request()->has('site')) {
         $rules['email'] = 'required|email';
       }
       else {
         $rules['name'] = 'required';
       }
       $rules['password'] = 'required';
        return $rules;
     }
     public function messages()
     {
         return [
           'email.required' => 'يجب ادخال البريد الالكترونى .',
           'email.email' => 'يجب ادخال بريد الكترونى صحيح .',
           'name.required' => 'يجب ادخال اسم المستخدم .',
           'password.required' => 'يجب ادخال كلمه السر .'
         ];
     }
}
