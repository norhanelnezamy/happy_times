<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Hall;

class HallOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if (!empty($request->route('hall'))) {
        $hall = Hall::findOrFail($request->route('hall'));
      }
      else {
        $hall = Hall::findOrFail(request()->hall_id);
      }
      if ($hall->user_id == Auth::user()->id) {
        return $next($request);
      }
      return response('unauthorized', 200);
    }
}
