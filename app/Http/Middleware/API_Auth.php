<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\User;

class API_Auth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = User::where('token', $request->route('token'))->first();
        if ($user) {
          Auth::guard('web')->login($user);
          return $next($request);
        }
        return response('unauthorized', 200);
    }
}
