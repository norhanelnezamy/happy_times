<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\HallRequest;

class HallReserver
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $hall_request = HallRequest::findOrFail($request->route('request'));
        if ($hall_request->user_id == Auth::user()->id) {
          return $next($request);
        }
        return response('unauthorized', 200);
    }
}
