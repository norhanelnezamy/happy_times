<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name'];
    
    
     public function halls()
    {
        return $this->hasMany('App\Hall','cat_id');
    }
}
