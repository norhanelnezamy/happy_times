<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Report extends Model
{
  public static function add($request){
    $report = new Report;
    $report->user_id = Auth::user()->id;
    $report->hall_id = $request->hall_id;
    $report->content = $request->content;
    $report->save();
  }
    
    
     public function hall()
    {
        return $this->belongsTo('App\Hall');
    }
    
     public function user()
    {
        return $this->belongsTo('App\User');
    }
}
