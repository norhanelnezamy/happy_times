<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'phone', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function add($request){
      $user = new User;
      $user->token = preg_replace('/[^A-Za-z0-9\-\']/', '', bcrypt(mt_rand()));
      $user->active_code = rand(111111,999999);
      $user->name = $request->name;
      $user->phone = $request->phone;
      $user->is_hall = $request->is_hall;
      $user->password = bcrypt($request->password);
      $user->save();
      return $user;
    }

    public static function edit($request){
      $user =  Auth::user();
      $user->name = $request->name;
      if (!empty($request->password)) {
        $user->password = bcrypt($request->password);
      }
      $user->save();
    }

    public static function active($request){
      $user = Auth::user();
      if ($user->active_code == $request->code ) {
        $user->active = 1;
        $user->save();
        return 1;
      }
      return 0;
    }

    public static function resend_active(){
      $user = Auth::user();
      $user->active_code = rand(111111,999999);
      $user->save();
    }
    
    
    
    public function halls()
    {
        return $this->hasMany('App\Hall');
    }
    
    
      public function reservations()
    {
        return $this->hasMany('App\HallRequest');
    }
}
