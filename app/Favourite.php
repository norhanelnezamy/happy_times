<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Favourite extends Model
{
  protected $table = 'favourites';
    public static function add($request){
      if (sizeof(Favourite::where('user_id', Auth::user()->id)->where('hall_id', $request->hall_id)->first()) == 0) {
        $favourite = new Favourite;
        $favourite->user_id = Auth::user()->id;
        $favourite->hall_id = $request->hall_id;
        $favourite->save();
      }
    }
}
